import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import * as io from 'socket.io-client';   // with ES6 import
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage implements OnInit {
  userdetails: any;
  notData: any;
  socket: any;
  // private count: number = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiCallerProvider,
    public events: Events) {
    this.userdetails = JSON.parse(localStorage.getItem('details'));
    // console.log("user details => " + JSON.stringify(this.userdetails));
    this.events.publish('cart:updated', 0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  itemSelected(item) {
    console.log("Notification clicked!")
  }

  ngOnInit() {
    this.getnotifications();
    this.socket = io('http://www.thingslab.in/sbNotifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected page');
      console.log("socket connected page ", this.socket.connected)
    });

    this.socket.on(this.userdetails._id, (msg) => {
      // console.log("tab push notify msg=> " + JSON.stringify(msg))
      this.notData.push(msg);
      // this.events.publish('cart:updated', ++this.count);

      console.log("tab notice data=> " + this.notData)
    });

    // this.socket.on("5b0bfedc43b09153a28c0b65", (msg) => {
    //   this.notData.push(msg);
    //   console.log("notice data=> " + this.notData)
    // })

  }

  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getnotifications();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 200);
  }

  getnotifications() {
    this.apiCall.getRecentNotifCall(this.userdetails._id)
      .subscribe(data => {
        // console.log("notification data => " + JSON.stringify(data));
        this.notData = data;
      },
        error => {
          console.log(error);
        });
  }

}
