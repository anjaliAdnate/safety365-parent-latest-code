import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { ConnectivityServiceProvider } from '../../providers/connectivity-service/connectivity-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private internetStatus: boolean = false;
  isParent: boolean = false;
  showPassword: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiCallerProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public connectivityService: ConnectivityServiceProvider,
    public events: Events) {
    this.checkInternetConnection();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  checkInternetConnection() {
    this.addConnectivityListeners();
    if (this.connectivityService.isOffline()) {
      this.internetStatus = true;
    }
  }
  addConnectivityListeners() {
    let onOnline = () => {
      this.internetStatus = false;
    };
    let onOffline = () => {
      this.internetStatus = true;
    };
    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);
  }

  userLogin(loginForm: NgForm) {
    var userData = {};
    var user = isNaN(loginForm.value.username);
    if (user == false) {
      userData = {
        "psd": loginForm.value.password,
        "ph_num": loginForm.value.username
      }
    } else {
      userData = {
        "psd": loginForm.value.password,
        "emailid": loginForm.value.username
      }
    }
    if (this.internetStatus == true) {
      let alert = this.alertCtrl.create({
        title: 'Network Alert',
        message: 'Please check your network connection and try again...',
        buttons: ['OK']
      });
      alert.present();
    }
    this.apiCall.startLoading().present();
    this.apiCall.loginCall(userData)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("response data token => ", data.token);
        this.events.publish("auth:token", data.token);
        localStorage.setItem("AuthToken", data.token);

        var logindata = JSON.stringify(data);
        var logindetails = JSON.parse(logindata);
        var userdetials = window.atob(logindetails.token.split('.')[1]);
        var details = JSON.parse(userdetials);
        localStorage.setItem('details', JSON.stringify(details));  // storing user details in localstorage

        const toast = this.toastCtrl.create({
          message: "Welcome! You're logged In successfully.",
          duration: 2000,
          position: 'top'
        });


        toast.onDidDismiss(() => {
          // console.log('Dismissed toast');
          localStorage.setItem("LOGGED_IN", "LOGGED_IN")
          this.navCtrl.setRoot("TabsPage")
        });
        toast.present();
      },
        error => {
          console.log(error)
          this.apiCall.stopLoading();
          var body = error._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: "Login Failed!",
            message: msg.message,
            buttons: ['Try Again']
          });
          alert.present();
        });
  }

}
