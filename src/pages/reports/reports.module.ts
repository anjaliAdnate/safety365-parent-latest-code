import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportsPage } from './reports';
import { SelectSearchableModule } from 'ionic-select-searchable';
@NgModule({
  declarations: [
    ReportsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportsPage),
    SelectSearchableModule
  ],
})
export class ReportsPageModule {}
